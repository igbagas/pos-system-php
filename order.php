<?php
    include_once'db/connect_db.php';
    session_start();
    if($_SESSION['username']==""){
        header('location:index.php');
    }else{
        if($_SESSION['role']=="Admin"){
          include_once'inc/header_all.php';
        }else{
            include_once'inc/header_all_operator.php';
        }
    }

    error_reporting(0);

    $id = $_GET['id'];

    $delete_query = "DELETE tbl_invoice , tbl_invoice_detail FROM tbl_invoice INNER JOIN tbl_invoice_detail ON tbl_invoice.invoice_id =
    tbl_invoice_detail.invoice_id WHERE tbl_invoice.invoice_id=$id";
    $delete = $pdo->prepare($delete_query);
    if($delete->execute()){
        echo'<script type="text/javascript">
            jQuery(function validation(){
            Swal.fire("Info", "Transaksi Telah Dihapus", "info", {
            button: "Continue",
                });
            });
            </script>';
    }
?>

<html>
<head>
<meta http-equiv="refresh" content="60">
</head>
</html>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content container-fluid">
        <div class="col-md-offset-1 col-md-10">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Daftar Penjualan</h3>
                    <a href="create_order.php" class="btn btn-primary btn-sm pull-right">
                    <span><i class="fa fa-plus"></i></span> Tambah Transaksi</a>
                </div>
                <div class="box-body">
                <div style="overflow-x:auto;">
                        <table class="table table-striped" id="myOrder">
                            <thead>
                                <tr>
                                    <th style="width:20px;">No</th>
                                    <th style="width:100px;">Petugas</th>
                                    <th style="width:100px;">Tanggal</th>
                                    <th style="width:100px;">Pendapatan</th>
                                    <th style="width:50px;">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $select = $pdo->prepare("SELECT * FROM tbl_invoice ORDER BY invoice_id DESC");
                                $select->execute();
                                while($row=$select->fetch(PDO::FETCH_OBJ)){
                                ?>
                                    <tr>
                                    <td><?php echo $no++ ; ?></td>
                                    <td class="text-uppercase"><?php echo $row->cashier_name; ?></td>
                                    <td><?php echo date("d F Y", strtotime($row->order_date)); ?></td>
                                    <td>Rp. <?php echo number_format($row->total); ?></td>
                                    <td>
                                        <?php if($_SESSION['role']=="Admin"){ ?>
                                        <a href="order.php?id=<?php echo $row->invoice_id; ?>" class="btn btn-danger btn-sm btn-del"><i class="fa fa-trash"></i></a>
                                        <?php } ?>
                                        <a href="misc/nota.php?id=<?php echo $row->invoice_id; ?>" target="_blank" class="btn btn-info btn-sm"><i class="fa fa-print"></i></a>
                                    </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                </div>

            </div>
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script>
    $('.btn-del').on('click', function(e){
        e.preventDefault();
        const href = $(this).attr('href')

        Swal.fire({
            title : 'Anda Yakin?',
            text : 'Setelah Dihapus Data Tidak Akan Bisa Kembali',
            type : 'warning',
            showCancelButton : true,
            confirmButtonColor : '#3085d6',
            cancelButtonColor : '#d33',
            confirmButtonText : 'Hapus Transaksi',
            cancelButtonText : 'Tidak'
        }).then((result) => {
            if(result.value){
                document.location.href=href;
            }
        })

    })
  </script>

    <script>
        $(document).ready( function () {
            $('#myOrder').DataTable();
        } );
    </script>

 <?php
    include_once'inc/footer_all.php';
 ?>