<?php
  include_once'db/connect_db.php';
  session_start();
  if($_SESSION['username']==""){
    header('location:index.php');
  }else{
    if($_SESSION['role']=="Admin"){
      include_once'inc/header_all.php';
    }else{
        include_once'inc/header_all_operator.php';
    }
  }

  if(isset($_POST['submit'])){

    $category = $_POST['category'];
    if(isset($_POST['category'])){

      $select = $pdo->prepare("SELECT cat_name FROM tbl_category WHERE cat_name='$category'");
      $select->execute();

      if($select->rowCount() > 0 ){
          echo'<script type="text/javascript">
              jQuery(function validation(){
              Swal.fire("Warning", "Kategori Sudah Ada", "warning", {
              button: "Continue",
                  });
              });
              </script>';
          }else{
            $insert = $pdo->prepare("INSERT INTO tbl_category(cat_name) VALUES(:category)");

            $insert->bindParam(':category', $category);

            if($insert->execute()){
              echo '<script type="text/javascript">
              jQuery(function validation(){
              Swal.fire("Success", "Kategori Baru Sudah Ditambahkan", "success", {
              button: "Continue",
                  });
              });
              </script>';
            }
          }
    }
  }
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content container-fluid">

      <div class="col-md-offset-1 col-md-10">
        <div class="col-md-6">
              <div class="box box-success">
                  <!-- /.box-header -->
                  <!-- form start -->
                  <form action="" method="POST">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="category">Nama Kategori</label>
                        <input type="text" class="form-control" name="category" placeholder="Nama Kategori" required>
                      </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-success" name="submit"><i class="fa fa-plus"></i> Tambah</button>
                    </div>
                  </form>
              </div>
        </div>
        <!-- Category Table -->
        <div class="col-md-6">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Kategori</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="overflow-x:auto;">
              <table class="table table-striped" id="">
                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Nama Kategori</th>
                          <th>Opsi</th>
                      </tr>

                  </thead>
                  <tbody>
                  <?php
                  $no = 1;
                  $select = $pdo->prepare('SELECT * FROM tbl_category');
                  $select->execute();
                  while($row=$select->fetch(PDO::FETCH_OBJ)){ ?>
                    <tr>
                      <td><?php echo $no++; ?></td>
                      <td><?php echo $row->cat_name; ?></td>
                      <td>
                          <a href="edit_category.php?id=<?php echo $row->cat_id; ?>"
                          class="btn btn-info btn-sm" name="btn_edit"><i class="fa fa-pencil"></i></a>
                          <a href="delete_category.php?id=<?php echo $row->cat_id; ?>"
                          class="btn btn-danger btn-sm btn-del" name="btn_delete"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  <?php
                  }
                  ?>

                  </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>


      </div>
       <!-- Category Form-->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
    $('.btn-del').on('click', function(e) {
      e.preventDefault();
      const href = $(this).attr('href')

      Swal.fire({
          title : 'Anda Yakin ?',
          text : 'Setelah Dihapus Data Tidak Akan Bisa Kembali',
          icon : 'warning',
          showCancelButton : true,
          confirmButtonColor : '#3085d6',
          cancelButtonColor : '#d33',
          confirmButtonText : 'Hapus Kategori',
          cancelButtonText : 'Tidak'
      }).then((result) => {
          if(result.value){
            document.location.href=href;
          }
      })
    })
  </script>


  <!-- DataTables Function -->
  <script>
  $(document).ready( function () {
      $('#myCategory').DataTable();
  } );
  </script>

<?php
  include_once'inc/footer_all.php';
?>